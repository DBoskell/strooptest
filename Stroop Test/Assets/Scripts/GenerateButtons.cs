using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GenerateButtons : MonoBehaviour
{
    // A simple script that generates the answer buttons for the player

    [Tooltip("The prefab for the buttons that you want to spawn in.")]
    public GameObject buttonPrefab;

    private IEnumerator Start()
    {
        // Waits until the GameManager's instance has been set before starting
        yield return new WaitUntil(() => GameManager.GMInstance != null);

        // Adds a listener to the GameManager's ToggleGame event
        GameManager.GMInstance.ToggleGame.AddListener(ToggleButtons);

        // Generates a button for each entry in the colours list attached to the current GameManager
        // Then changes the text on the button to correspond with the given colour
        foreach(GameManager.StroopColours color in GameManager.GMInstance.colours)
        {
            GameObject obj = Instantiate(buttonPrefab, transform);
            obj.GetComponentInChildren<TextMeshProUGUI>().text = color.colorName;
        }
    }

    public void ToggleButtons()
    {
        gameObject.GetComponent<ToggleUI>().Toggle();
    }
}
