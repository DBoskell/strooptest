using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using TMPro;

public class GameManager : MonoBehaviour
{
    // Manages various aspects of the game
    public static GameManager GMInstance;

    // New UnityEvent that allows me to handle the player's choice of button more easily
    public class GMEvent : UnityEvent { }
    public GMEvent AnswerChosen = new GMEvent();
    public GMEvent ToggleGame = new GMEvent();

    [System.Serializable]
    public struct StroopColours
    {
        public Color color;
        public string colorName;
    }

    [Tooltip("The colours available in the game. Choose from the colour-picker and give it a name to display on the buttons.")]
    public List<StroopColours> colours;

    [Tooltip("The text that displays whether or not the player got the correct answer.")]
    public TextMeshProUGUI answerText;

    [Tooltip("The text that displays how much time a player has left each round.")]
    public TextMeshProUGUI timerText;

    // The current correct answer, changes every time a new colour is generated
    private string m_currentColour;
    public string CurrentColour
    {
        get
        {
            return m_currentColour;
        }
        set
        {
            m_currentColour = value;
        }
    }

    // The player's score, this is determined by how much time is left when a correct answer is chosen
    private float m_score;
    public float Score
    {
        get
        {
            return m_score;
        }
        private set
        {
            m_score = value;
        }
    }

    // Variables handling the timer
    private float m_timer;
    private float m_timerMax = 15f;
    private bool m_timerRunning = false;

    // Variables relating to the amount of rounds in a game
    private int m_maxRounds = 10;
    private int m_currentRound = 0;
    public int CurrentRound
    {
        get
        {
            return m_currentRound;
        }
    }

    // Variables related to the results screen at the end of the game
    private int m_correctAnswers = 0;
    public int CorrectAnswers
    {
        get
        {
            return m_correctAnswers;
        }
    }
    private int m_incorrectAnswers = 0;
    public int IncorrectAnswers
    {
        get
        {
            return m_incorrectAnswers;
        }
    }

    private void Start()
    {
        // Set the static instance of the GameManager to this
        GMInstance = this;
    }

    private void Update()
    {
        // If the timer is running, counts down until the timer reaches 0 and turns it off
        // The player receives 0 points for a correct answer after this point
        if (m_timerRunning)
        {
            m_timer -= Time.deltaTime;
            timerText.text = $"{m_timer}";
            if (m_timer <= 0)
            {
                m_timer = 0;
                timerText.text = "0";
                m_timerRunning = false;
            }
        }
    }

    public void CloseGame()
    {
        Application.Quit();
    }

    public void ChangeScene()
    {
        // If we are in the main menu scene, moves to the game scene
        // Otherwise moves back to the main menu scene
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByBuildIndex(0))
        {
            SceneManager.LoadScene(1);
        }
        else
        {
            SceneManager.LoadScene(0);
        }
    }

    public void CheckAnswer(string colour)
    {
        if (colour == CurrentColour)
        {
            // Add to the score based on the time remaining
            AddToScore(m_timer);

            // Show some UI that says the player is correct
            answerText.text = "Correct!";
            answerText.color = Color.green;
            m_correctAnswers++;

        }
        else
        {
            // Show some UI that says the player is incorrect
            answerText.text = "Incorrect!";
            answerText.color = Color.red;
            m_incorrectAnswers++;
        }

        // Add to the round counter, update the scores and check that it wasn't the final round
        m_currentRound++;
        ScoreManager.SMInstance.UpdateScores();
        if (m_currentRound == m_maxRounds)
        {
            EndGame();
        }
        else
        {
            // Reset the timer and generate a new colour
            AnswerChosen.Invoke();
            StartTimer();
        }
    }

    private void AddToScore(float number)
    {
        Score += number;
    }

    private void StartTimer()
    {
        m_timer = m_timerMax;
        m_timerRunning = true;
    }

    public void BeginGame()
    {
        m_currentRound = 0;
        m_correctAnswers = 0;
        m_incorrectAnswers = 0;
        StartTimer();
        ToggleGame.Invoke();
        AnswerChosen.Invoke();
    }

    private void EndGame()
    {
        m_timerRunning = false;
        timerText.text = "";
        answerText.text = "";

        ToggleGame.Invoke();
    }
}
