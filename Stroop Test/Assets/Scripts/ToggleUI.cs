using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class ToggleUI : MonoBehaviour
{
    // A simple script to toggle UI on the main menu
    // Makes use of Canvas Groups to make things visible and interactable as needed.
    private CanvasGroup m_canvasGroup;
    [SerializeField][Tooltip("Set this to true for the UI that you want to display upon startup, leave as false for everything else.")]
    private bool m_isActive;


    void Start()
    {
        // Start by finding the CanvasGroup component
        m_canvasGroup = GetComponent<CanvasGroup>();

        // Then set the starting UI to be visible and interactable while setting all others to be invisible
        if (m_isActive)
        {
            m_canvasGroup.interactable = m_isActive;
            m_canvasGroup.blocksRaycasts = m_isActive;
            m_canvasGroup.alpha = 1;
        }
        else
        {
            m_canvasGroup.interactable = m_isActive;
            m_canvasGroup.blocksRaycasts = m_isActive;
            m_canvasGroup.alpha = 0;
        }
    }

    public void Toggle()
    {
        // Flip the value of m_isActive
        m_isActive = !m_isActive;

        // Change the interactable and blocksRaycasts values to m_isActive
        m_canvasGroup.interactable = m_isActive;
        m_canvasGroup.blocksRaycasts = m_isActive;

        // Set the alpha based on the value of m_isActive
        if (m_isActive)
        {
            m_canvasGroup.alpha = 1;
        }
        else
        {
            m_canvasGroup.alpha = 0;
        }
    }
}
