using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    // Tracks and displays the score for the game
    [Tooltip("The text box that displays how many rounds were played")]
    public TextMeshProUGUI roundsText;
    [Tooltip("The text box that displays how many correct answers there were")]
    public TextMeshProUGUI correctAnswersText;
    [Tooltip("The text box that displays how many incorrect answers there were")]
    public TextMeshProUGUI incorrectAnswersText;
    [Tooltip("The text box that displays how much the player scored")]
    public TextMeshProUGUI scoreText;

    public static ScoreManager SMInstance;

    private IEnumerator Start()
    {
        // Wait until the GMInstance variable is no longer null
        yield return new WaitUntil(() => GameManager.GMInstance != null);

        // Set the instance to this
        SMInstance = this;

        // Add the ToggleScore function as a listener to the ToggleGame event in GameManager
        GameManager.GMInstance.ToggleGame.AddListener(ToggleScore);
    }

    public void UpdateScores()
    {
        // Update the text boxes with the requisite details after every round
        roundsText.text = $"Rounds Played: {GameManager.GMInstance.CurrentRound}";
        correctAnswersText.text = $"Correct Answers: {GameManager.GMInstance.CorrectAnswers}";
        incorrectAnswersText.text = $"Incorrect Answers: {GameManager.GMInstance.IncorrectAnswers}";
        scoreText.text = $"Final Score: {GameManager.GMInstance.Score}";
    }

    private void ToggleScore()
    {
        gameObject.GetComponent<ToggleUI>().Toggle();
    }
}
