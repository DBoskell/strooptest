using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ChooseButton : MonoBehaviour
{
    // Begins the logic for determining if the player's choice was correct or not
    // This exists because the buttons, as prefabs, cannot reference anything in the scene
    // It also helps to tell the GameManager which button was pressed, which is an added bonus

    public void ButtonChosen()
    {
        GameManager.GMInstance.CheckAnswer(GetComponentInChildren<TextMeshProUGUI>().text);
    }
}
