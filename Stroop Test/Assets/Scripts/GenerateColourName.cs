using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GenerateColourName : MonoBehaviour
{
    // Randomly Generates the colour that will show in the text on-screen
    private TextMeshProUGUI m_text;

    private IEnumerator Start()
    {
        yield return new WaitUntil(() => GameManager.GMInstance != null);
        // Set the m_text variable
        m_text = GetComponent<TextMeshProUGUI>();

        // Add the Generate function as a listener to the AnswerChosen Event in GameManager
        GameManager.GMInstance.AnswerChosen.AddListener(Generate);

        // Add the ToggleColour function as a listener to the ToggleGame Event in GameManager
        GameManager.GMInstance.ToggleGame.AddListener(ToggleColour);
    }

    public void Generate()
    {
        // First generate what the text will say
        int nameNumber = Random.Range(0, GameManager.GMInstance.colours.Count);
        m_text.text = GameManager.GMInstance.colours[nameNumber].colorName;

        // Then generate the colour and compare with the text to make sure they are not the same
        int colourNumber = Random.Range(0, GameManager.GMInstance.colours.Count);
        bool identical = false;

        if (nameNumber == colourNumber)
        {
            identical = true;
        }

        // If they are the same, loop until colourNumber becomes something different
        while (identical == true)
        {
            colourNumber = Random.Range(0, GameManager.GMInstance.colours.Count);

            if (nameNumber != colourNumber)
            {
                identical = false;
            }
        }

        // Finally, set the colour of the text
        m_text.color = GameManager.GMInstance.colours[colourNumber].color;
        GameManager.GMInstance.CurrentColour = GameManager.GMInstance.colours[colourNumber].colorName;
    }

    private void ToggleColour()
    {
        gameObject.GetComponentInParent<ToggleUI>().Toggle();
    }
}
